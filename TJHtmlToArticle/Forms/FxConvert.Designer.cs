﻿namespace TJHtmlToArticle
{
    partial class FxConvert
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FxConvert));
      this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
      this.PvManagement = new Telerik.WinControls.UI.RadPageView();
      this.PgDownload = new Telerik.WinControls.UI.RadPageViewPage();
      this.TxWebPageAddress = new Telerik.WinControls.UI.RadButtonTextBox();
      this.BxClearWebPageAddress = new Telerik.WinControls.UI.RadButtonElement();
      this.LxDownload = new Telerik.WinControls.UI.RadLabel();
      this.BxDownload = new Telerik.WinControls.UI.RadButton();
      this.PgFolder = new Telerik.WinControls.UI.RadPageViewPage();
      this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
      this.TxFolderDestination = new Telerik.WinControls.UI.RadButtonTextBox();
      this.BxFolderDestination = new Telerik.WinControls.UI.RadButtonElement();
      this.PgConvertHtmlToText = new Telerik.WinControls.UI.RadPageViewPage();
      this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
      this.BxConvertAllFilesInTheFolder = new Telerik.WinControls.UI.RadButton();
      this.TxFolderSource = new Telerik.WinControls.UI.RadButtonTextBox();
      this.radButtonElement1 = new Telerik.WinControls.UI.RadButtonElement();
      this.radPanel2 = new Telerik.WinControls.UI.RadPanel();
      this.TxMessage = new Telerik.WinControls.UI.RadTextBox();
      this.BxClearMessage = new Telerik.WinControls.UI.RadButton();
      ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
      this.radPanel1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.PvManagement)).BeginInit();
      this.PvManagement.SuspendLayout();
      this.PgDownload.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.TxWebPageAddress)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.LxDownload)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxDownload)).BeginInit();
      this.PgFolder.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.TxFolderDestination)).BeginInit();
      this.PgConvertHtmlToText.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxConvertAllFilesInTheFolder)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.TxFolderSource)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).BeginInit();
      this.radPanel2.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.TxMessage)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxClearMessage)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
      this.SuspendLayout();
      // 
      // radPanel1
      // 
      this.radPanel1.Controls.Add(this.PvManagement);
      this.radPanel1.Dock = System.Windows.Forms.DockStyle.Top;
      this.radPanel1.Location = new System.Drawing.Point(0, 0);
      this.radPanel1.Name = "radPanel1";
      this.radPanel1.Size = new System.Drawing.Size(949, 172);
      this.radPanel1.TabIndex = 0;
      // 
      // PvManagement
      // 
      this.PvManagement.Controls.Add(this.PgDownload);
      this.PvManagement.Controls.Add(this.PgFolder);
      this.PvManagement.Controls.Add(this.PgConvertHtmlToText);
      this.PvManagement.Dock = System.Windows.Forms.DockStyle.Fill;
      this.PvManagement.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.PvManagement.ItemSizeMode = Telerik.WinControls.UI.PageViewItemSizeMode.EqualWidth;
      this.PvManagement.Location = new System.Drawing.Point(0, 0);
      this.PvManagement.Name = "PvManagement";
      this.PvManagement.SelectedPage = this.PgDownload;
      this.PvManagement.Size = new System.Drawing.Size(949, 172);
      this.PvManagement.TabIndex = 4;
      ((Telerik.WinControls.UI.RadPageViewStripElement)(this.PvManagement.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
      ((Telerik.WinControls.UI.RadPageViewStripElement)(this.PvManagement.GetChildAt(0))).ItemSizeMode = Telerik.WinControls.UI.PageViewItemSizeMode.EqualWidth;
      // 
      // PgDownload
      // 
      this.PgDownload.Controls.Add(this.TxWebPageAddress);
      this.PgDownload.Controls.Add(this.LxDownload);
      this.PgDownload.Controls.Add(this.BxClearMessage);
      this.PgDownload.Controls.Add(this.BxDownload);
      this.PgDownload.ItemSize = new System.Drawing.SizeF(209F, 28F);
      this.PgDownload.Location = new System.Drawing.Point(10, 37);
      this.PgDownload.Name = "PgDownload";
      this.PgDownload.Size = new System.Drawing.Size(928, 124);
      this.PgDownload.Text = "Скачивание";
      this.PgDownload.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // TxWebPageAddress
      // 
      this.TxWebPageAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.TxWebPageAddress.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.TxWebPageAddress.Location = new System.Drawing.Point(12, 37);
      this.TxWebPageAddress.Name = "TxWebPageAddress";
      this.TxWebPageAddress.RightButtonItems.AddRange(new Telerik.WinControls.RadItem[] {
            this.BxClearWebPageAddress});
      this.TxWebPageAddress.Size = new System.Drawing.Size(905, 24);
      this.TxWebPageAddress.TabIndex = 3;
      // 
      // BxClearWebPageAddress
      // 
      this.BxClearWebPageAddress.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
      this.BxClearWebPageAddress.EnableBorderHighlight = true;
      this.BxClearWebPageAddress.EnableFocusBorder = true;
      this.BxClearWebPageAddress.EnableFocusBorderAnimation = true;
      this.BxClearWebPageAddress.EnableHighlight = true;
      this.BxClearWebPageAddress.EnableRippleAnimation = true;
      this.BxClearWebPageAddress.Image = ((System.Drawing.Image)(resources.GetObject("BxClearWebPageAddress.Image")));
      this.BxClearWebPageAddress.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
      this.BxClearWebPageAddress.Margin = new System.Windows.Forms.Padding(2, 0, 0, 0);
      this.BxClearWebPageAddress.Name = "BxClearWebPageAddress";
      this.BxClearWebPageAddress.Text = "Clear";
      // 
      // LxDownload
      // 
      this.LxDownload.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.LxDownload.Location = new System.Drawing.Point(16, 16);
      this.LxDownload.Name = "LxDownload";
      this.LxDownload.Size = new System.Drawing.Size(138, 18);
      this.LxDownload.TabIndex = 2;
      this.LxDownload.Text = "Адрес веб-страницы";
      // 
      // BxDownload
      // 
      this.BxDownload.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.BxDownload.Location = new System.Drawing.Point(12, 79);
      this.BxDownload.Name = "BxDownload";
      this.BxDownload.Size = new System.Drawing.Size(208, 30);
      this.BxDownload.TabIndex = 1;
      this.BxDownload.Text = "Скачать веб-страницу";
      // 
      // PgFolder
      // 
      this.PgFolder.Controls.Add(this.radLabel2);
      this.PgFolder.Controls.Add(this.TxFolderDestination);
      this.PgFolder.ItemSize = new System.Drawing.SizeF(209F, 28F);
      this.PgFolder.Location = new System.Drawing.Point(10, 37);
      this.PgFolder.Name = "PgFolder";
      this.PgFolder.Size = new System.Drawing.Size(928, 124);
      this.PgFolder.Text = "Папка";
      this.PgFolder.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // radLabel2
      // 
      this.radLabel2.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.radLabel2.Location = new System.Drawing.Point(16, 16);
      this.radLabel2.Name = "radLabel2";
      this.radLabel2.Size = new System.Drawing.Size(470, 18);
      this.radLabel2.TabIndex = 6;
      this.radLabel2.Text = "Папка для записи текстовых файлов, преобразованных из веб-страниц";
      // 
      // TxFolderDestination
      // 
      this.TxFolderDestination.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.TxFolderDestination.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.TxFolderDestination.Location = new System.Drawing.Point(12, 37);
      this.TxFolderDestination.Name = "TxFolderDestination";
      this.TxFolderDestination.RightButtonItems.AddRange(new Telerik.WinControls.RadItem[] {
            this.BxFolderDestination});
      this.TxFolderDestination.Size = new System.Drawing.Size(905, 0);
      this.TxFolderDestination.TabIndex = 4;
      // 
      // BxFolderDestination
      // 
      this.BxFolderDestination.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
      this.BxFolderDestination.EnableBorderHighlight = true;
      this.BxFolderDestination.EnableFocusBorder = true;
      this.BxFolderDestination.EnableFocusBorderAnimation = true;
      this.BxFolderDestination.EnableHighlight = true;
      this.BxFolderDestination.EnableRippleAnimation = true;
      this.BxFolderDestination.Image = ((System.Drawing.Image)(resources.GetObject("BxFolderDestination.Image")));
      this.BxFolderDestination.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
      this.BxFolderDestination.Margin = new System.Windows.Forms.Padding(2, 0, 0, 0);
      this.BxFolderDestination.Name = "BxFolderDestination";
      this.BxFolderDestination.Text = "Clear";
      this.BxFolderDestination.UseCompatibleTextRendering = false;
      // 
      // PgConvertHtmlToText
      // 
      this.PgConvertHtmlToText.Controls.Add(this.radLabel3);
      this.PgConvertHtmlToText.Controls.Add(this.BxConvertAllFilesInTheFolder);
      this.PgConvertHtmlToText.Controls.Add(this.TxFolderSource);
      this.PgConvertHtmlToText.ItemSize = new System.Drawing.SizeF(209F, 28F);
      this.PgConvertHtmlToText.Location = new System.Drawing.Point(10, 37);
      this.PgConvertHtmlToText.Name = "PgConvertHtmlToText";
      this.PgConvertHtmlToText.Size = new System.Drawing.Size(928, 124);
      this.PgConvertHtmlToText.Text = " Преобразование html в текст       ";
      this.PgConvertHtmlToText.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // radLabel3
      // 
      this.radLabel3.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.radLabel3.Location = new System.Drawing.Point(16, 16);
      this.radLabel3.Name = "radLabel3";
      this.radLabel3.Size = new System.Drawing.Size(826, 18);
      this.radLabel3.TabIndex = 9;
      this.radLabel3.Text = "Папка, в которой находятся исходные данные: html-файлы, которые будут преобразова" +
    "ны в текстовые файлы без html-тэгов";
      // 
      // BxConvertAllFilesInTheFolder
      // 
      this.BxConvertAllFilesInTheFolder.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.BxConvertAllFilesInTheFolder.Location = new System.Drawing.Point(12, 79);
      this.BxConvertAllFilesInTheFolder.Name = "BxConvertAllFilesInTheFolder";
      this.BxConvertAllFilesInTheFolder.Size = new System.Drawing.Size(208, 30);
      this.BxConvertAllFilesInTheFolder.TabIndex = 8;
      this.BxConvertAllFilesInTheFolder.Text = "Конвертировать html-файлы";
      // 
      // TxFolderSource
      // 
      this.TxFolderSource.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.TxFolderSource.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.TxFolderSource.Location = new System.Drawing.Point(12, 37);
      this.TxFolderSource.Name = "TxFolderSource";
      this.TxFolderSource.RightButtonItems.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement1});
      this.TxFolderSource.Size = new System.Drawing.Size(905, 0);
      this.TxFolderSource.TabIndex = 7;
      // 
      // radButtonElement1
      // 
      this.radButtonElement1.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
      this.radButtonElement1.EnableBorderHighlight = true;
      this.radButtonElement1.EnableFocusBorder = true;
      this.radButtonElement1.EnableFocusBorderAnimation = true;
      this.radButtonElement1.EnableHighlight = true;
      this.radButtonElement1.EnableRippleAnimation = true;
      this.radButtonElement1.Image = ((System.Drawing.Image)(resources.GetObject("radButtonElement1.Image")));
      this.radButtonElement1.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
      this.radButtonElement1.Margin = new System.Windows.Forms.Padding(2, 0, 0, 0);
      this.radButtonElement1.Name = "radButtonElement1";
      this.radButtonElement1.Text = "Clear";
      this.radButtonElement1.UseCompatibleTextRendering = false;
      // 
      // radPanel2
      // 
      this.radPanel2.Controls.Add(this.TxMessage);
      this.radPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.radPanel2.Location = new System.Drawing.Point(0, 172);
      this.radPanel2.Name = "radPanel2";
      this.radPanel2.Size = new System.Drawing.Size(949, 512);
      this.radPanel2.TabIndex = 0;
      // 
      // TxMessage
      // 
      this.TxMessage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.TxMessage.AutoSize = false;
      this.TxMessage.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.TxMessage.Location = new System.Drawing.Point(11, 11);
      this.TxMessage.Multiline = true;
      this.TxMessage.Name = "TxMessage";
      this.TxMessage.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.TxMessage.Size = new System.Drawing.Size(927, 490);
      this.TxMessage.TabIndex = 0;
      // 
      // BxClearMessage
      // 
      this.BxClearMessage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.BxClearMessage.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.BxClearMessage.Location = new System.Drawing.Point(709, 79);
      this.BxClearMessage.Name = "BxClearMessage";
      this.BxClearMessage.Size = new System.Drawing.Size(208, 30);
      this.BxClearMessage.TabIndex = 1;
      this.BxClearMessage.Text = "Очистить список сообщений";
      // 
      // FxConvert
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(949, 684);
      this.Controls.Add(this.radPanel2);
      this.Controls.Add(this.radPanel1);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Name = "FxConvert";
      // 
      // 
      // 
      this.RootElement.ApplyShapeToControl = true;
      this.Text = "FxConvert";
      ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
      this.radPanel1.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.PvManagement)).EndInit();
      this.PvManagement.ResumeLayout(false);
      this.PgDownload.ResumeLayout(false);
      this.PgDownload.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.TxWebPageAddress)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.LxDownload)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxDownload)).EndInit();
      this.PgFolder.ResumeLayout(false);
      this.PgFolder.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.TxFolderDestination)).EndInit();
      this.PgConvertHtmlToText.ResumeLayout(false);
      this.PgConvertHtmlToText.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxConvertAllFilesInTheFolder)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.TxFolderSource)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).EndInit();
      this.radPanel2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.TxMessage)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxClearMessage)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
      this.ResumeLayout(false);

        }

    #endregion

    private Telerik.WinControls.UI.RadPanel radPanel1;
    private Telerik.WinControls.UI.RadPanel radPanel2;
    private Telerik.WinControls.UI.RadTextBox TxMessage;
    private Telerik.WinControls.UI.RadButton BxDownload;
    private Telerik.WinControls.UI.RadLabel LxDownload;
    private Telerik.WinControls.UI.RadButtonTextBox TxWebPageAddress;
    public Telerik.WinControls.UI.RadButtonElement BxClearWebPageAddress;
    private Telerik.WinControls.UI.RadPageView PvManagement;
    private Telerik.WinControls.UI.RadPageViewPage PgDownload;
    private Telerik.WinControls.UI.RadPageViewPage PgFolder;
    private Telerik.WinControls.UI.RadButtonTextBox TxFolderDestination;
    public Telerik.WinControls.UI.RadButtonElement BxFolderDestination;
    private Telerik.WinControls.UI.RadPageViewPage PgConvertHtmlToText;
    private Telerik.WinControls.UI.RadLabel radLabel2;
    private Telerik.WinControls.UI.RadButtonTextBox TxFolderSource;
    public Telerik.WinControls.UI.RadButtonElement radButtonElement1;
    private Telerik.WinControls.UI.RadLabel radLabel3;
    private Telerik.WinControls.UI.RadButton BxConvertAllFilesInTheFolder;
    private Telerik.WinControls.UI.RadButton BxClearMessage;
  }
}
