﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using TJFramework;
using static TJFramework.TJFrameworkManager;
using static TJHtmlToArticle.Program;

namespace TJHtmlToArticle
{
  public partial class FxConvert : RadForm, IEventStartWork
  {
    public StringUtilities WxTool { get; } = new StringUtilities();

    public FxConvert()
    {
      InitializeComponent();
    }

    public string GetFolderSource() => TxFolderSource.Text;

    public string GetFolderDestination() => TxFolderDestination.Text;

    public void EventStartWork()
    {
      Manager.Introduce(this);
      TJFrameworkManager.Service.SetIcons(this.Icon);

      BxDownload.Click += EventDownloadWebPage;
      BxConvertAllFilesInTheFolder.Click += EventConvertAllFilesInTheFolder;
      BxClearMessage.Click += (s, e) => TxMessage.Clear();

      TxWebPageAddress.MouseMove += EventWebPageMouseMove;
      BxClearWebPageAddress.Click += EventClearWebPageAddress;
      TxWebPageAddress.ZzRemoveButtonBorder();
      TxFolderSource.ZzRemoveButtonBorder();
      TxFolderDestination.ZzRemoveButtonBorder();

      TxFolderDestination.Text = Program.ApplicationSettings.FolderDestination;
      TxFolderSource.Text = Program.ApplicationSettings.FolderSource;

      TxFolderSource.ZzOpenFolderHandler(true, true, () => Program.ApplicationSettings.FolderSource = TxFolderSource.Text);
      TxFolderDestination.ZzOpenFolderHandler(true, true, () => Program.ApplicationSettings.FolderDestination = TxFolderDestination.Text);
      PvManagement.SelectedPage = PgDownload;
    }

    private void InnerPrint(string arg)
    {
      if (TxMessage.Text.Length > 100000) TxMessage.Clear();
      TxMessage.AppendText(DateTime.Now.ToString("yyyyMMdd HH:mm:ss") + "  " + arg + Environment.NewLine);
    }

    public void Print(string message)
    {
      if (TxMessage.InvokeRequired)
      {
        TxMessage.Invoke
          (
            (MethodInvoker)delegate
              {
                InnerPrint(message);
              }
          );
      }
      else
      {
        InnerPrint(message);
      }
    }

    private async void EventClearWebPageAddress(object sender, EventArgs e)
    {
      BxClearWebPageAddress.Visibility = ElementVisibility.Collapsed;
      TxWebPageAddress.Clear(); await Task.Delay(1000);
      BxClearWebPageAddress.Visibility = ElementVisibility.Visible;
    }

    private void EventWebPageMouseMove(object sender, MouseEventArgs e)
    {
      if (Program.ApplicationSettings.AutoAddUrlFromClipboard == false) return;
      if (String.IsNullOrWhiteSpace(TxWebPageAddress.Text) == false) return;
      if (Clipboard.ContainsText(TextDataFormat.Text) == false) return;

      string clipboardText = Clipboard.GetText(TextDataFormat.Text); Clipboard.Clear();
      if (String.IsNullOrWhiteSpace(clipboardText)) return;
      if (WxTool.IsValidUrl(clipboardText) == false) return;

      TxWebPageAddress.Text = clipboardText;
      if (Program.ApplicationSettings.AutoStartDownload) EventDownloadWebPage(sender, e);
    }

    public async void EventDownloadWebPage(object sender, EventArgs e)
    {
      BxDownload.Enabled = false;
      string url = TxWebPageAddress.Text.Trim(); TxWebPageAddress.Clear();
      Manager.RequestFileDownload(url);
      Ms.ShortMessage(MsgType.Debug, "Запрос принят", 190, TxWebPageAddress).Offset(0, -100).Delay(2).NoTable().Create();      
      await Task.Delay(1000);
      BxDownload.Enabled = true;
    }

    public void EventConvertAllFilesInTheFolder(object sender, EventArgs e)
    {
      BxConvertAllFilesInTheFolder.Enabled = false;
      TxMessage.Clear();
      Manager.ConvertAllFilesInTheFolder(GetFolderSource());
      //BxConvertAllFilesInTheFolder.Enabled = true;
    }

    public void EventConvertAllFilesInTheFolderCompeted()
    {
      BxConvertAllFilesInTheFolder.Enabled = true;
      Print("Обработка файлов завершена.");
      Print("----------------------------------------------------------------------------------");
    }
  }
}
