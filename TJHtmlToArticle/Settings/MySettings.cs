﻿using System;
using System.ComponentModel;
using System.Drawing;
using Telerik.WinControls.UI;
using TJFramework;
using TJFramework.ApplicationSettings;
using static TJFramework.Logger.Manager;
using static TJHtmlToArticle.Program;

namespace TJHtmlToArticle
{
  [Serializable]
  public class MySettings : TJStandardApplicationSettings
  {
    [Category("Основные настройки")]
    [DisplayName("Автоматически добавлять URL из буфера обмена")]
    [RadSortOrder(1)]
    [Description("Автоматически добавлять URL из буфера обмена")]
    public bool AutoAddUrlFromClipboard { get; set; } = false;

    [Category("Основные настройки")]
    [DisplayName("Проверять валидность URL")]
    [RadSortOrder(2)]
    [Description("Проверять валидность URL")]
    public bool CheckUrl { get; set; } = false;

    [Category("Основные настройки")]
    [DisplayName("Автоматически начинать закачку URL")]
    [RadSortOrder(3)]
    [Description("Веб страница, добавленная из буфера обмена начнёт скачиваться сразу")]
    public bool AutoStartDownload { get; set; } = false;

    [Category("Основные настройки")]
    [DisplayName("Через сколько минут возобновить неудачную закачку")]
    [RadSortOrder(4)]
    [RadRange(0,1000)]
    [Description("Если не удалось скачать страницу, то закачка возобновится через указанное число минут.")]
    public int MinutesTryToDownloadAgain { get; set; } = 5;

    [Category("Основные настройки")]
    [DisplayName("Сколько раз пытаться возобновить неудачную закачку")]
    [RadSortOrder(5)]
    [RadRange(0, 100)]
    [Description("Если не удалось скачать страницу, то закачка возобновится столько раз, сколько указано в этой настройке.")]
    public int NumberTryToDownloadAgain { get; set; } = 5;

    [Category("Расположение папок")]
    [DisplayName("Сохранение результата")]
    [RadSortOrder(1)]
    [Description("")]
    [ReadOnly(true)]
    public string FolderDestination { get; set; } = string.Empty;

    [Category("Расположение папок")]
    [DisplayName("Исходные html-файлы")]
    [RadSortOrder(1)]
    [Description("")]
    [ReadOnly(true)]
    public string FolderSource { get; set; } = string.Empty;

    [Category("Интерфейс")]
    [DisplayName("Проигрывать звук начала закачки")]
    [RadSortOrder(1)]
    public bool PlaySoundDownloadStart { get; set; } = false;

    [Category("Интерфейс")]
    [DisplayName("Проигрывать звук окончания закачки")]
    [RadSortOrder(2)]
    public bool PlaySoundDownloadEnd { get; set; } = false;


    [Category("Звуковые файлы")]
    [DisplayName("Звук начала закачки")]
    [RadSortOrder(1)]
    [ReadOnly(true)]
    public string SoundDownloadStart { get; set; } = "TJ_Sound_Started.wav";

    [Category("Звуковые файлы")]
    [DisplayName("Звук окончания закачки")]
    [RadSortOrder(2)]
    [ReadOnly(true)]
    public string SoundDownloadEnd { get; set; } = "TJ_Sound_Completed.wav";

    public override void PropertyValueChanged(string PropertyName)
    {
      //Manager.EventPropertyValueChanged(PropertyName);
      //ms.Message(MessageType.msg_info, property_name, "Changed!", 4,MessagePosition.pos_SC); 
      //Log.Save(MsgType.Debug, "public override void PropertyValueChanged(string PropertyName)", PropertyName);
      /*if (PropertyName == nameof(MainPageOrientation))
        TJFrameworkManager.Service.SetMainPageViewOrientation(MainPageOrientation);*/
    }

    public override void EventBeforeSaving()
    {
      //Password = "";
      //Log.Save(MsgType.Debug, "public override void EventBeforeSaving()", "test");
      //MessageBox.Show("EventBefore_Saving");
    }

    public override void EventAfterSaving()
    {
      //Password = "12345";
      //Log.Save(MsgType.Debug, "public override void EventAfterSaving()", "test");
      //MessageBox.Show("EventAfter_Saving");
    }
  }
}