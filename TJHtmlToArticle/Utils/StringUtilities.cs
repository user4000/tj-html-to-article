﻿using System;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using TJFramework;
using TJFramework.Tools;
using static TJFramework.TJFrameworkManager;
using static TJFramework.Logger.Manager;

namespace TJHtmlToArticle
{
  public class StringUtilities
  {
    public string NewLine { get; } = "\r\n\r\n\r\n";

    public string TagTitle { get; } = "title";

    public bool IsValidUrl(string url)
    {
      if (Program.ApplicationSettings.CheckUrl == false) return true;
      Uri uriResult;
      return Uri.TryCreate(url, UriKind.Absolute, out uriResult)
      && (
          (uriResult.Scheme == Uri.UriSchemeHttp) ||
          (uriResult.Scheme == Uri.UriSchemeHttps) ||
          (uriResult.Scheme == Uri.UriSchemeFtp) ||
          (uriResult.Scheme == Uri.UriSchemeFile)
         );
    }

    public string RemoveSpecialCharacters(string text)
    {
      text = text.Replace('.', '_').Replace(' ', '_');
      return Regex.Replace(text, "[^\\w\\._]", "");
    }

    public string ConvertHtmlToPlainText(string html)
    {
      html = HtmlUtilities.ConvertToPlainText(html);
      return Regex.Replace(html, @"[\r\n]{4,}", NewLine);
    }

    public string ExtractTextFromHtmlTag(string html, string tag)
    {
      HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
      doc.LoadHtml(html);

      //Log.Save(MsgType.Debug, "document text", doc.Text);

      HtmlNodeCollection nodes = doc.DocumentNode.SelectNodes("//" + tag);
      if (nodes == null)
      {
        return string.Empty;
      }

      foreach (HtmlNode node in nodes)
      {
        html = node.InnerText; break; 
      }
      return html;
    }

    public AmTextFile CreateTextFileFromHtml(string html, string webPage, string author)
    {
      string title = ExtractTextFromHtmlTag(html, TagTitle);
      if ( String.IsNullOrWhiteSpace(title))
      {
        title= $"Tag {TagTitle} not found " + Guid.NewGuid().ToString().StringLeft(10);
      }
      title = RemoveSpecialCharacters(title);
      html = ConvertHtmlToPlainText(html);
      return AmTextFile.Create(title, html, webPage, author);
    }
  }
}
