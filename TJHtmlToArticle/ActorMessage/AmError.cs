﻿using System;

namespace TJHtmlToArticle
{
  public class AmError
  {
    public Exception Error { get; } = null;

    public string Author { get; } = string.Empty;

    public string WebPageAddress { get; } = string.Empty;

    public AmError(Exception error, string webPage, string author)
    {
      Error = error;
      WebPageAddress = webPage;
      Author = author;
    }

    public static AmError Create(Exception error, string webPage, string author) => new AmError(error, webPage, author);
  }
}
