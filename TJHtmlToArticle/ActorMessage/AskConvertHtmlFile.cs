﻿namespace TJHtmlToArticle
{
  public class AskConvertHtmlFile
  {
    public string FileName { get; } = string.Empty;

    public AskConvertHtmlFile(string text)
    {
      FileName = text;
    }

    public static AskConvertHtmlFile Create(string text) => new AskConvertHtmlFile(text);
  }
}
  
