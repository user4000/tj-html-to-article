﻿namespace TJHtmlToArticle
{
  public class AmTextFile
  {
    public string Title { get; } = string.Empty;

    public string Content { get; } = string.Empty;

    public string Author { get; } = string.Empty;

    public string WebPageAddress { get; } = string.Empty;

    public AmTextFile(string title, string content, string webPage, string author)
    {
      Title = title;
      Content = content;
      WebPageAddress = webPage;
      Author = author;
    }

    public static AmTextFile Create(string title, string content, string webPage, string author) 
      => new AmTextFile(title, content, webPage, author);
  }
}
