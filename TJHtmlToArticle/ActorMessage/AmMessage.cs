﻿using System;

namespace TJHtmlToArticle
{
  public class AmMessage
  {
    public static string MsgWebPageHasBeenAlreadyDownloaded { get; } = "Указанная веб-страница уже была закачана. Повторная обработка отменена.";

    public string Header { get; } = string.Empty;

    public string Message { get; } = string.Empty;

    public string Author { get; } = string.Empty;

    public AmMessage(string header, string message, string author)
    {
      Header = header; Message = message; Author = author;
    }

    public static AmMessage Create(string header, string message, string author) => new AmMessage(header, message, author);
  }
}
