﻿namespace TJHtmlToArticle
{
  public class AmDownloadedFilesCount
  {
    public int CountRequest { get; set; }

    public int CountDownloaded { get; set; }

    public AmDownloadedFilesCount(int countRequest, int countDownloaded)
    {
      CountRequest = countRequest; CountDownloaded = countDownloaded;
    }

    public static AmDownloadedFilesCount Create(int countRequest, int countDownloaded) => new AmDownloadedFilesCount(countRequest, countDownloaded);
  }
}
