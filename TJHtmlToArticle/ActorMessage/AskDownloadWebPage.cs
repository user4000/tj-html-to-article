﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TJHtmlToArticle
{
  public class AskDownloadWebPage
  {
    public string WebPageAddress { get; } = string.Empty;

    public AskDownloadWebPage(string text)
    {
      WebPageAddress = text;
    }

    public static AskDownloadWebPage Create(string text) => new AskDownloadWebPage(text);
  }
}
