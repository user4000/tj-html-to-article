﻿using System;
using Akka.Actor;

namespace TJHtmlToArticle
{
  public class AcCommander : ReceiveActor
  {
    private CxManager Manager { get; set; } = null;

    private IActorRef AxWebPageDownloader { get; set; } = null;

    private IActorRef AxHtmlConverter { get; set; } = null;

    private string MyName { get; } = nameof(AcCommander);

    public AcCommander()
    {
      Receive<CxManager>(obj => EventApplicationManager(obj));

      Receive<AskDownloadWebPage>(obj => EventDownloadWebPage(obj));
      Receive<AskConvertHtmlFile>(obj => EventConvertHtmlFile(obj));
      Receive<AskAllFilesWereProcessed>(obj => EventAllFilesWereProcessed(obj));

      Receive<AmTextFile>(obj => ResponseFileConverted(obj));
      Receive<AmAllFilesWereProcessed>(obj => ResponseAllFilesInFolderWasProcessed(obj));
      Receive<AmError>(obj => ResponseAnErrorHasOccured(obj));
      Receive<AmMessage>(obj => ResponseMessage(obj));
      Receive<AmDownloadedFilesCount>(obj => ResponseDownloadedFilesCount(obj));

      CreateChildActors();
    }

    private void CreateChildActors()
    {
      AxWebPageDownloader = Context.ActorOf<AcWebPageDownloader>(nameof(AcWebPageDownloader));
      AxHtmlConverter = Context.ActorOf<AcHtmlFileConverter>(nameof(AcHtmlFileConverter));
    }

    private void EventAllFilesWereProcessed(AskAllFilesWereProcessed obj)
    {
      AxHtmlConverter.Tell(obj);
    }

    private void ResponseAllFilesInFolderWasProcessed(AmAllFilesWereProcessed response)
    {
      Manager.ResponseConvertAllFilesInTheFolderCompleted();
    }

    private void ResponseFileConverted(AmTextFile response)
    {
      Manager.ResponseFileConverted(response);
    }

    private void EventApplicationManager(CxManager obj)
    {
      if (Manager == null) Manager = obj;
    }

    private void EventConvertHtmlFile(AskConvertHtmlFile obj)
    {
      AxHtmlConverter.Tell(obj);
    }

    private void EventDownloadWebPage(AskDownloadWebPage obj)
    {
      AxWebPageDownloader.Tell(obj);
      Manager.ResponseMessage(AmMessage.Create("Принят запрос на скачивание веб-страницы:", obj.WebPageAddress, MyName));
    }

    private void ResponseAnErrorHasOccured(AmError response)
    {
      Manager.ResponseAnErrorHasOccured(response);
    }

    private void ResponseMessage(AmMessage response)
    {
      Manager.ResponseMessage(response);
    }

    private void ResponseDownloadedFilesCount(AmDownloadedFilesCount obj)
    {
      Manager.ResponseDownloadedFilesCount(obj);
    }
  }
}
