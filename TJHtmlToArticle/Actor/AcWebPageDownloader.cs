﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using Akka.Actor;

namespace TJHtmlToArticle
{
  public class AcWebPageDownloader : ReceiveActor
  {
    private WebClient WxClient { get; } = new WebClient();

    private StringUtilities WxTool { get; } = new StringUtilities();

    private string MyName { get; } = string.Empty;

    private HashSet<string> WebPagesSuccess { get; } = new HashSet<string>();

    private HashSet<string> WebPagesRequest { get; } = new HashSet<string>();

    public AcWebPageDownloader()
    {
      Receive<AskDownloadWebPage>(msg => EventDownloadWebPage(msg));
      WxClient.Encoding = Encoding.UTF8;
      MyName = nameof(AcWebPageDownloader);
      var version = Environment.OSVersion.Version;
      if (version < new Version(6, 2))
      {
        System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
      }
    }

    private bool CheckUri(string address)
    {
      bool result = true;
      try
      {
        Uri uri = new Uri(address);
      }
      catch(Exception ex)
      {
        result = false;
        Exception IncorrectUri = new Exception("Error! URI is incorrect. " + ex.Message);
        AmError error = AmError.Create(IncorrectUri, address, MyName);
        Sender.Tell(error);
      }
      return result;
    }

    private void EventDownloadWebPage(AskDownloadWebPage msg)
    {
      string address = msg.WebPageAddress.Trim();
      if (!CheckUri(address)) return;

      if (ThisUrlHasBeenAlreadyProcessed(address)) return; 
      try
      {
        Uri uri = new Uri(address);
        Sender.Tell(AmMessage.Create("Начинаю закачивать страницу", address, MyName));
        string html = WxClient.DownloadString(uri);
        AmTextFile result = WxTool.CreateTextFileFromHtml(html, address, MyName);
        Sender.Tell(result);
        WebPagesSuccess.Add(address);
        if (WebPagesSuccess.Count > 100000) WebPagesSuccess.Clear();
        if (WebPagesRequest.Count > 100000) WebPagesRequest.Clear();
      }
      catch(Exception ex)
      {
        AmError error = AmError.Create(ex, address, MyName);
        Sender.Tell(error);
      }
      Sender.Tell(AmDownloadedFilesCount.Create(WebPagesRequest.Count, WebPagesSuccess.Count));
    }

    private bool ThisUrlHasBeenAlreadyProcessed(string address)
    {
      if (WebPagesSuccess.Contains(address))
      {
        Sender.Tell(AmMessage.Create(AmMessage.MsgWebPageHasBeenAlreadyDownloaded, address, MyName));
        return true;
      }

      if (!WebPagesRequest.Contains(address))
      {
        WebPagesRequest.Add(address);
      }

      return false;
    }
  }
}

