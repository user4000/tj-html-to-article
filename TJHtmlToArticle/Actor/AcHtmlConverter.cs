﻿using System;
using System.IO;
using Akka.Actor;

namespace TJHtmlToArticle
{
  public class AcHtmlFileConverter : ReceiveActor
  {
    public StringUtilities WxTool { get; } = new StringUtilities();

    private string MyName { get; } = string.Empty;

    public AcHtmlFileConverter()
    {
      Receive<AskConvertHtmlFile>(msg => EventConvertWebPageToPlainText(msg));
      Receive<AskAllFilesWereProcessed>(obj => EventAllFilesWereProcessed(obj));
      MyName = nameof(AcHtmlFileConverter);
    }

    private void EventAllFilesWereProcessed(AskAllFilesWereProcessed obj)
    {
      Sender.Tell(new AmAllFilesWereProcessed());
    }

    private void EventConvertWebPageToPlainText(AskConvertHtmlFile msg)
    {
      try
      {
        string html = File.ReadAllText(msg.FileName);
        AmTextFile result = WxTool.CreateTextFileFromHtml(html, msg.FileName, MyName);
        Sender.Tell(result);
      }
      catch (Exception ex)
      {
        Sender.Tell(AmError.Create(ex, msg.FileName, MyName));
      }
    }
  }
}
