﻿using System.Threading;

namespace TJHtmlToArticle
{
  public class DownloadByTimer
  {
    public CxManager Manager { get; set; } = null;

    public FxConvert Form { get; set; } = null;

    public string Address { get; set; } = null;

    public System.Threading.Timer timer { get; set; } = null;

    private DownloadByTimer()
    {

    }

    public static DownloadByTimer Start(CxManager manager, FxConvert form, string address, int dueTime)
    {
      DownloadByTimer item = new DownloadByTimer();
      item.Manager = manager;
      item.Form = form;
      item.Address = address;
      item.timer = new System.Threading.Timer(new TimerCallback(item.EventTimerTick), null, dueTime, Timeout.Infinite);
      return item;
    }

    public void EventTimerTick(object state)
    {
      Form.Print($"Попробую скачать страницу ещё раз = {Address}");
      Manager.RequestFileDownload(Address);
      timer.Change(Timeout.Infinite, Timeout.Infinite);
      timer.Dispose();
      Manager = null;
      Form = null;
      Address = null;
    }
  }
}
