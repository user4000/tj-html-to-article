﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Media;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Akka.Actor;
using TJFramework;
using TJFramework.Tools;
using static TJFramework.Logger.Manager;
using static TJFramework.TJFrameworkManager;
using static TJHtmlToArticle.Program;

namespace TJHtmlToArticle
{
  public class CxManager
  {
    public IActorRef AxCommander { get; set; } = null;

    public FxConvert FormConvert { get; private set; } = null;

    private SoundPlayer PlayerStart { get; set; } = null;

    private SoundPlayer PlayerEnd { get; set; } = null;

    public ConcurrentDictionary<string, int> DownloadAttempts { get; } = new ConcurrentDictionary<string, int>();

    public ConcurrentDictionary<string, Timer> Timers { get; } = new ConcurrentDictionary<string, Timer>();

    private CxManager()
    {
      if (AcSystem == null) throw new Exception("Error! Do not create manager before creating Actor System!");
      AxCommander = AcSystem.ActorOf<AcCommander>(nameof(AcCommander));
      if (AxCommander == null) throw new Exception("Error! Commander is null!");
    }

    private void InitSoundPlayers()
    {
      try
      {
        PlayerStart = new SoundPlayer(Program.ApplicationSettings.SoundDownloadStart);
      }
      catch
      {
        PlayerStart = null;
      }

      try
      {
        PlayerEnd = new SoundPlayer(Program.ApplicationSettings.SoundDownloadEnd);
      }
      catch
      {
        PlayerEnd = null;
      }
    }

    public void Introduce(FxConvert form)
    {
      if (FormConvert == null)
      {
        FormConvert = form; // Знакомим менеджера с формой FxConvert //     
      }
      InitSoundPlayers();
    }

    public static CxManager Create()
    {
      CxManager manager = new CxManager();
      manager.AxCommander.Tell(manager);
      return manager;
    }

    public void PlaySoundStart()
    {
      if ((!Program.ApplicationSettings.PlaySoundDownloadStart) || (PlayerStart == null)) return;
      try
      {
        PlayerStart.Play();
      }
      catch
      {
        PlayerStart = null;
      }
    }

    public void PlaySoundEnd()
    {
      if ((!Program.ApplicationSettings.PlaySoundDownloadEnd) || (PlayerEnd == null)) return;
      try
      {
        PlayerEnd.Play();
      }
      catch
      {
        PlayerEnd = null;
      }
    }

    public void RequestFileDownload(string WebPageAddress)
    {
      AxCommander.Tell(AskDownloadWebPage.Create(WebPageAddress));
      PlaySoundStart();
    }

    public void RequestHtmlFileConvertToPlainTextFile(string FileName)
    {
      AxCommander.Tell(AskConvertHtmlFile.Create(FileName));
    }

    public void RequestConvertAllFilesInTheFolderCompleted()
    {
      AxCommander.Tell(new AskAllFilesWereProcessed());
    }

    internal void ResponseDownloadedFilesCount(AmDownloadedFilesCount obj)
    {
      if (obj.CountDownloaded==obj.CountRequest)
        Ms.Message($"Всего начато закачек = {obj.CountRequest}", $"Из них выполнено = {obj.CountDownloaded}").NoAlert().Ok();
      else
        Ms.Message($"Всего начато закачек = {obj.CountRequest}", $"Из них выполнено = {obj.CountDownloaded}; не выполнено = {obj.CountRequest-obj.CountDownloaded}").NoAlert().Debug();
    }

    public void ResponseConvertAllFilesInTheFolderCompleted()
    {
      FormConvert.EventConvertAllFilesInTheFolderCompeted();
    }

    public void ConvertAllFilesInTheFolder(string FolderName)
    {
      string[] files = Directory.GetFiles(FolderName, "*.htm*");
      foreach (var file in files)
      {
        FormConvert.Print($"Файл отправлен на конвертирование: {file}");
        RequestHtmlFileConvertToPlainTextFile(file);
      }
      RequestConvertAllFilesInTheFolderCompleted();
    }

    public void ResponseFileConverted(AmTextFile response)
    {
      string FileName = Regex.Replace(response.Title, @"[_]{2,}", "_").StringLeft(200);
      FileName = Path.Combine(FormConvert.GetFolderDestination(), FileName) + ".txt";
      if (File.Exists(FileName)) File.Delete(FileName);
      string message = $"---- Файл записан: {response.WebPageAddress} = " + FileName;
      
      try
      {
        File.WriteAllText(FileName, response.Content);
        Ms.Message("Создан файл", FileName).NoAlert().Info();
      }
      catch (Exception ex)
      {
        message = $"Ошибка! Файл не был записан: {FileName}\r\n\r\n{ex.Message}";
        Ms.Error($"Ошибка при попытке сохранения файла {FileName}", ex).ToFile().NoAlert().Error();
      }
      FormConvert.Print(message);
      if (response.Author == nameof(AcWebPageDownloader))
      {
        PlaySoundEnd();
      }
    }

    internal void ResponseMessage(AmMessage response)
    {
      FormConvert.Print($"{response.Header} {response.Message}");
      Ms.Message(response.Header, response.Message).NoAlert().Info();
    }

    internal void ResponseAnErrorHasOccured(AmError response)
    {
      string address = response.WebPageAddress;

      if (response.Author == nameof(AcWebPageDownloader))
      {
        FormConvert.Print($"Не удалось скачать веб-страницу {address}");
        Ms.Message("Не удалось скачать веб-страницу", address).NoAlert().Fail();
      }

      FormConvert.Print($"Ошибка! {response.Author} --- {response.Error.Message} {response.Error.StackTrace}");

      if (response.Author != nameof(AcWebPageDownloader)) return;

      if (Program.ApplicationSettings.MinutesTryToDownloadAgain < 1) return;

      if (DownloadAttempts.Count > 10000)
      {
        DownloadAttempts.Clear();
      }

      if (DownloadAttempts.TryGetValue(address, out int PreviousAttempts))
      {
        if (PreviousAttempts >= Program.ApplicationSettings.NumberTryToDownloadAgain)
        {
          //DownloadAttempts.TryRemove(address, out int x);
          return;
        }
      }

      DownloadAttempts.AddOrUpdate(address, 1, (key, value) => value + 1); 
      int dueTime = Program.ApplicationSettings.MinutesTryToDownloadAgain * 60000;
      DownloadByTimer timer = DownloadByTimer.Start(this, FormConvert, address, dueTime);
    }
  }
}
