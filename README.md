# TJ-Html-To-Article

Save html web page as a plain-text file.

With this program, you can download web pages in the form of ordinary text files, which can then be read on a tablet using reading programs.

To download a web page, just insert its address in the input field and click the "Download Web Page" button.

There is also a mode for converting already downloaded web pages to text files.